package com.example.demo.Controlers;

import com.example.demo.Entities.WeeklyReportEntity;
import com.example.demo.Service.WeeklyReportEntityService;
import com.example.demo.models.DataSentToTimeTrackingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("/api/crud")
public class CRUDControler {

    private final WeeklyReportEntityService weeklyReportService;

    @Autowired
    public CRUDControler(WeeklyReportEntityService weeklyReportService) {
        this.weeklyReportService = weeklyReportService;
    }

    @PostMapping("/postTimeCard")
    public ResponseEntity<String> postReport(@RequestBody DataSentToTimeTrackingService report) {
        WeeklyReportEntity weeklyReport = new WeeklyReportEntity();
        weeklyReport.setUserId(report.getId());
        weeklyReport.setSelectedOption(report.getReport().getSelectedOption());
        weeklyReport.setStartDate(report.getReport().getStartDate());
        weeklyReport.setEndDate(report.getReport().getEndDate());
        weeklyReport.setMonday(report.getReport().getMonday());
        weeklyReport.setTuesday(report.getReport().getTuesday());
        weeklyReport.setWednesday(report.getReport().getWednesday());
        weeklyReport.setThursday(report.getReport().getThursday());
        weeklyReport.setFriday(report.getReport().getFriday());
        weeklyReport.setMondayMention(report.getReport().getMondayMention());
        weeklyReport.setTuesdayMention(report.getReport().getTuesdayMention());
        weeklyReport.setWednesdayMention(report.getReport().getWednesdayMention());
        weeklyReport.setThursdayMention(report.getReport().getThursdayMention());
        weeklyReport.setFridayMention(report.getReport().getFridayMention());

        weeklyReportService.saveWeeklyReport(weeklyReport);
        return ResponseEntity.ok("weekly report has been published");
    }
}
