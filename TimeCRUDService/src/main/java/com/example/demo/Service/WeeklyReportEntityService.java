package com.example.demo.Service;

import com.example.demo.Entities.WeeklyReportEntity;
import com.example.demo.reposirories.WeeklyReportEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeeklyReportEntityService {
    private final WeeklyReportEntityRepository weeklyReportEntityRepository;

    @Autowired
    public WeeklyReportEntityService(WeeklyReportEntityRepository weeklyReportRepository) {
        this.weeklyReportEntityRepository = weeklyReportRepository;
    }

    public void saveWeeklyReport(WeeklyReportEntity weeklyReport) {
        weeklyReportEntityRepository.save(weeklyReport);
    }
}
