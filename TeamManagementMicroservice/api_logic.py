from datetime import datetime
from typing import List

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from app import db
from app.models import Team, TeamMember, WorkLog

def add_member(name: str, email: str, role: str, permissions: List[str]) -> TeamMember:
    member = TeamMember(name=name, email=email, role=role, permissions=permissions)
    try:
        db.session.add(member)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        raise ValueError('Email already exists')

    return member

def delete_member(member_id: int) -> None:
    member = TeamMember.query.get_or_404(member_id)
    db.session.delete(member)
    db.session.commit()

def update_member(member_id: int, name: str = None, email: str = None, role: str = None, permissions: List[str] = None) -> TeamMember:
    member = TeamMember.query.get_or_404(member_id)

    if name is not None:
        member.name = name
    if email is not None:
        member.email = email
    if role is not None:
        member.role = role
    if permissions is not None:
        member.permissions = permissions

    db.session.commit()

    return member

def get_members() -> List[TeamMember]:
    members = TeamMember.query.all()
    return members

def add_work_log(member_id: int, task_id: int, project_id: int, hours: float, productivity: str) -> WorkLog:
    work_log = WorkLog(member_id=member_id, task_id=task_id, project_id=project_id, hours=hours, productivity=productivity)
    db.session.add(work_log)
    db.session.commit()

    return work_log

def get_work_logs(member_id: int, start_date: datetime, end_date: datetime) -> List[WorkLog]:
    work_logs = WorkLog.query.filter_by(member_id=member_id).filter(WorkLog.timestamp.between(start_date, end_date)).all()
    return work_logs

def get_team_summary() -> dict:
    team_summary = {}
    team = Team.query.first()
    team_members = TeamMember.query.filter(TeamMember.id.in_(team.members)).all()

    total_hours = 0
    total_productivity = 0
    for member in team_members:
        work_logs = WorkLog.query.filter_by(member_id=member.id).all()
        member_hours = sum(work_log.hours for work_log in work_logs)
        total_hours += member_hours

        member_productivity = sum(int(work_log.productivity) for work_log in work_logs)
        total_productivity += member_productivity

    team_summary['total_hours'] = total_hours
    team_summary['total_productivity'] = total_productivity

    return team_summary
