from app import db

class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    members = db.Column(db.ARRAY(db.Integer), nullable=False, default=[])

class TeamMember(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)
    role = db.Column(db.String(64), nullable=False)
    permissions = db.Column(db.ARRAY(db.String(64)), nullable=False, default=[])

class WorkLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, db.ForeignKey('team_member.id'), nullable=False)
    task_id = db.Column(db.Integer, nullable=False)
    project_id = db.Column(db.Integer, nullable=False)
    hours = db.Column(db.Float, nullable=False)
    productivity = db.Column(db.String(64), nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
