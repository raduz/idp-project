from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://username:password@localhost/yourdb'
db = SQLAlchemy(app)

class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    members = db.Column(db.ARRAY(db.Integer), nullable=False)

class TeamMember(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(120), nullable=False, unique=True)
    role = db.Column(db.String(50), nullable=False)
    permissions = db.Column(db.ARRAY(db.String), nullable=False)

class WorkLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer, nullable=False)
    task_id = db.Column(db.Integer, nullable=False)
    project_id = db.Column(db.Integer, nullable=False)
    hours = db.Column(db.Float, nullable=False)
    productivity = db.Column(db.String(50), nullable=False)

# Endpoint for adding a team member
@app.route('/team/members', methods=['POST'])
def add_member():
    name = request.json['name']
    email = request.json['email']
    role = request.json['role']
    permissions = request.json['permissions']

    member = TeamMember(name=name, email=email, role=role, permissions=permissions)
    db.session.add(member)
    db.session.commit()

    return jsonify({'id': member.id, 'name': member.name, 'email': member.email, 'role': member.role, 'permissions': member.permissions}), 201

# Endpoint for removing a team member
@app.route('/team/members/<int:member_id>', methods=['DELETE'])
def delete_member(member_id):
    member = TeamMember.query.get_or_404(member_id)
    db.session.delete(member)
    db.session.commit()
    return '', 204

# Endpoint for updating a team member's details
@app.route('/team/members/<int:member_id>', methods=['PATCH'])
def update_member(member_id):
    member = TeamMember.query.get_or_404(member_id)

    if 'name' in request.json:
        member.name = request.json['name']
    if 'email' in request.json:
        member.email = request.json['email']
    if 'role' in request.json:
        member.role = request.json['role']
    if 'permissions' in request.json:
        member.permissions = request.json['permissions']

    db.session.commit()

    return jsonify({'id': member.id, 'name': member.name, 'email': member.email, 'role': member.role, 'permissions': member.permissions})

# Endpoint for getting a list of team members
@app.route('/team/members', methods=['GET'])
def get_members():
    members = TeamMember.query.all()
    return jsonify([{'id': member.id, 'name': member.name, 'email': member.email, 'role': member.role, 'permissions': member.permissions} for member in members])

# Endpoint for assigning roles and permissions to a team member
@app.route('/team/members/<int:member_id>/roles', methods=['PUT'])
def update_member_roles(member_id):
    member = TeamMember.query.get_or_404(member_id)

    if 'role' in request.json:
        member.role = request.json['role']
    if 'permissions' in request.json:
        member.permissions = request.json['permissions']

    db.session.commit
