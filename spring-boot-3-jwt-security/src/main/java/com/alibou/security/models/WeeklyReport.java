package com.alibou.security.models;

import java.util.LinkedHashMap;

public class WeeklyReport {
    private String selectedOption;
    private String startDate;
    private String endDate;
    private int monday;
    private int tuesday;
    private int wednesday;
    private int thursday;
    private int friday;
    private String mondayMention;
    private String tuesdayMention;
    private String wednesdayMention;
    private String thursdayMention;
    private String fridayMention;

    // Getters and setters

    public WeeklyReport(String selectedOption, String startDate, String endDate, int monday, int tuesday, int wednesday, int thursday, int friday, String mondayMention, String tuesdayMention, String wednesdayMention, String thursdayMention, String fridayMention) {
        this.selectedOption = selectedOption;
        this.startDate = startDate;
        this.endDate = endDate;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.mondayMention = mondayMention;
        this.tuesdayMention = tuesdayMention;
        this.wednesdayMention = wednesdayMention;
        this.thursdayMention = thursdayMention;
        this.fridayMention = fridayMention;
    }

    @Override
    public String toString() {
        return "WeeklyReport{" +
                "selectedOption=" + selectedOption +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", monday=" + monday +
                ", tuesday=" + tuesday +
                ", wednesday=" + wednesday +
                ", thursday=" + thursday +
                ", friday=" + friday +
                ", mondayMention='" + mondayMention + '\'' +
                ", tuesdayMention='" + tuesdayMention + '\'' +
                ", wednesdayMention='" + wednesdayMention + '\'' +
                ", thursdayMention='" + thursdayMention + '\'' +
                ", fridayMention='" + fridayMention + '\'' +
                '}';
    }

    public static int valConverter(Object s){
        if (s instanceof String) {
            return Integer.parseInt((String) s);
        } else {
            return ((Integer)s).intValue();
        }
    }

    public WeeklyReport(LinkedHashMap<String, Object> reportRaw) {
        if (reportRaw instanceof LinkedHashMap) {
            LinkedHashMap<String, Object> data = (LinkedHashMap<String, Object>) reportRaw;
            this.selectedOption = (String) data.get("selectedOption");
            this.startDate = (String) data.get("startDate");
            this.endDate = (String) data.get("endDate");
            this.monday = valConverter( data.get("monday"));
            this.tuesday = valConverter( data.get("tuesday"));
            this.wednesday = valConverter( data.get("wednesday"));
            this.thursday = valConverter( data.get("thursday"));
            this.friday = valConverter(  data.get("friday"));
            this.mondayMention = (String) data.get("mondayMention");
            this.tuesdayMention = (String) data.get("tuesdayMention");
            this.wednesdayMention = (String) data.get("wednesdayMention");
            this.thursdayMention = (String) data.get("thursdayMention");
            this.fridayMention = (String) data.get("fridayMention");
        } else {
            throw new IllegalArgumentException("Invalid data type for reportRaw");
        }
    }

    public String getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getMonday() {
        return monday;
    }

    public void setMonday(int monday) {
        this.monday = monday;
    }

    public int getTuesday() {
        return tuesday;
    }

    public void setTuesday(int tuesday) {
        this.tuesday = tuesday;
    }

    public int getWednesday() {
        return wednesday;
    }

    public void setWednesday(int wednesday) {
        this.wednesday = wednesday;
    }

    public int getThursday() {
        return thursday;
    }

    public void setThursday(int thursday) {
        this.thursday = thursday;
    }

    public int getFriday() {
        return friday;
    }

    public void setFriday(int friday) {
        this.friday = friday;
    }

    public String getMondayMention() {
        return mondayMention;
    }

    public void setMondayMention(String mondayMention) {
        this.mondayMention = mondayMention;
    }

    public String getTuesdayMention() {
        return tuesdayMention;
    }

    public void setTuesdayMention(String tuesdayMention) {
        this.tuesdayMention = tuesdayMention;
    }

    public String getWednesdayMention() {
        return wednesdayMention;
    }

    public void setWednesdayMention(String wednesdayMention) {
        this.wednesdayMention = wednesdayMention;
    }

    public String getThursdayMention() {
        return thursdayMention;
    }

    public void setThursdayMention(String thursdayMention) {
        this.thursdayMention = thursdayMention;
    }

    public String getFridayMention() {
        return fridayMention;
    }

    public void setFridayMention(String fridayMention) {
        this.fridayMention = fridayMention;
    }
}
