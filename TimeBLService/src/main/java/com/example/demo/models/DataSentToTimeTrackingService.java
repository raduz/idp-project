package com.example.demo.models;

import java.util.LinkedHashMap;

public class DataSentToTimeTrackingService {
    private WeeklyReport report;

    private Integer id;

    @Override
    public String toString() {
        return "DataSentToTimeTrackingService{" +
                "report=" + report +
                ", id=" + id +
                '}';
    }

    public DataSentToTimeTrackingService(WeeklyReport report, Integer id) {
        this.report = report;
        this.id = id;
    }

    public DataSentToTimeTrackingService(LinkedHashMap<String, Object> recievedData){
        if (recievedData instanceof LinkedHashMap) {
            this.id = (Integer) recievedData.get("id");
            this.report = new WeeklyReport((LinkedHashMap<String, Object>)recievedData.get("report"));
        }
        else {
            throw new IllegalArgumentException("Invalid data type for reportRaw");
        }
    }


    public WeeklyReport getReport() {
        return report;
    }

    public void setReport(WeeklyReport report) {
        this.report = report;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
