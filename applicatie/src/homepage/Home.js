const Home = () =>{
    return (
    <div>Our project is inspired from a web-based platform designed to help users track their
    working hours efficiently. Users can log in, clock in and out and monitor their time spent on
    various tasks and projects. The platform offers features such as a user-friendly dashboard,
    detailed reports and the ability to set and track goals. It also includes team management
    capabilities, enabling team leads to monitor work hours and productivity.</div>)
}
export default Home;