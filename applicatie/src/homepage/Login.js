import React, { useState } from 'react';

const LoginForm = () => {
  const [formData, setFormData] = useState({
    email: '',
    password: ''
  });

  const saveToken = () => {
    const token = formData.email;
    localStorage.setItem('token', token);
    console.log('Token saved to local storage');
  };

  const handleInputChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    });
  }

  const handleSubmit = (event) => {
    saveToken();
    event.preventDefault();
    fetch('http://localhost:8080/api/v1/auth/authenticate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    })
    .then(response => response.json())
    .then(data => {
      const extractedData = data.access_token;
      const newAccessToken = extractedData; 
      sessionStorage.setItem('access_token', newAccessToken);
    })
    .catch(error => console.error(error));
  }

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="email">Email:</label>
      <input type="email" name="email" onChange={handleInputChange} required />
      
      <label htmlFor="password">Password:</label>
      <input type="password" name="password" onChange={handleInputChange} required />
      
      <button type="submit">Log In</button>
    </form>
  );
}

export default LoginForm;