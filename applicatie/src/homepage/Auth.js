import LoginForm from "./Login"
import SignupForm from "./SignUp";

const Auth = () =>{
    
    return(
        <>
            <LoginForm/>
            <SignupForm/>
        </>
    );
}

export default Auth;