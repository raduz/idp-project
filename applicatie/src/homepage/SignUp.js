import React, { useState } from 'react';

const SignupForm = () => {
  const [formData, setFormData] = useState({
    firstname: '',
    lastname: '',
    email: '',
    password: ''
  });

  const handleInputChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    fetch('/api/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch(error => console.error(error));
  }

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="firstname">First Name:</label>
      <input type="text" name="firstname" onChange={handleInputChange} required />
      
      <label htmlFor="lastname">Last Name:</label>
      <input type="text" name="lastname" onChange={handleInputChange} required />
      
      <label htmlFor="email">Email:</label>
      <input type="email" name="email" onChange={handleInputChange} required />
      
      <label htmlFor="password">Password:</label>
      <input type="password" name="password" onChange={handleInputChange} required />
      
      <button type="submit">Sign Up</button>
    </form>
  );
}

export default SignupForm;