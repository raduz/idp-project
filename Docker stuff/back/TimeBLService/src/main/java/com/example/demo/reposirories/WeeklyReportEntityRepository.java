package com.example.demo.reposirories;

import com.example.demo.Entities.WeeklyReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WeeklyReportEntityRepository extends JpaRepository<WeeklyReportEntity, Integer> {

    @Query("SELECT w FROM WeeklyReportEntity w WHERE w.userId = :userId")
    List<WeeklyReportEntity> findAllByUserId(@Param("userId") Integer userId);
    WeeklyReportEntity findByUserId(Integer userId);
}
