package com.example.demo.models;


import com.example.demo.Entities.WeeklyReportEntity;

import java.util.List;

public class GetTimeAdminDto {
    List<WeeklyReportEntity> list;
    public List<WeeklyReportEntity> getList() {
        return list;
    }

    public void setList(List<WeeklyReportEntity> list) {
        this.list = list;
    }

    public GetTimeAdminDto(List<WeeklyReportEntity> list) {
        this.list = list;

    }
    public GetTimeAdminDto() {
        // Default constructor
    }
}
