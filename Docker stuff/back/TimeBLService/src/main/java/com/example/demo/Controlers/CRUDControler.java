package com.example.demo.Controlers;

import com.example.demo.Entities.WeeklyReportEntity;
import com.example.demo.models.DataSentToTimeTrackingService;
import com.example.demo.models.GetTimeAdminDto;
import com.example.demo.reposirories.WeeklyReportEntityRepository;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/api/bl")
public class CRUDControler {

    private final WeeklyReportEntityRepository weeklyReportEntityRepository;

    @Autowired
    public CRUDControler(WeeklyReportEntityRepository weeklyReportEntityRepository) {
        this.weeklyReportEntityRepository = weeklyReportEntityRepository;
    }


    @PostMapping("/postTimeCard")
    public ResponseEntity<String> postReport(@RequestBody DataSentToTimeTrackingService report) {

        String url = "http://localhost:8082/api/crud/postTimeCard"; // Replace with the URL of the other Spring app

        // Set request headers

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
//        DataSentToTimeTrackingService data = new DataSentToTimeTrackingService(report)
        HttpEntity<DataSentToTimeTrackingService> requestEntity = new HttpEntity<>(report, headers); // Pass requestBody here if needed

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

        String responseBody = response.getBody();

        // Process the response as needed
        return ResponseEntity.ok(responseBody);
    }
    @GetMapping("/getTime/{userId}")
    public ResponseEntity<Integer> getTime(@PathVariable("userId") Integer userId) {
        List<WeeklyReportEntity> reports = weeklyReportEntityRepository.findAllByUserId(userId);

        Integer sum = 0;
        for (WeeklyReportEntity report : reports) {
            sum += report.getMonday() + report.getTuesday() + report.getWednesday() + report.getThursday() + report.getFriday();
        }
        return ResponseEntity.ok(sum);
    }

    @GetMapping("/getAllTime")
    public ResponseEntity<GetTimeAdminDto> getTime() {
        List<WeeklyReportEntity> list = weeklyReportEntityRepository.findAll();
        GetTimeAdminDto dto = new GetTimeAdminDto(list);

        return ResponseEntity.ok(dto);
    }


}
