package com.example.demo.reposirories;

import com.example.demo.Entities.WeeklyReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeeklyReportEntityRepository extends JpaRepository<WeeklyReportEntity, Integer> {
    // ... custom query methods can be defined here if needed
}
