package com.example.demo.Entities;


import javax.persistence.*;

@Entity
@Table(name = "weekly_reports")
public class WeeklyReportEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private Integer userId;

    public Integer getId() {
        return id;
    }

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "timecard_id")
    private Integer timecardId;

    public int getUserId() {
        return userId;
    }

    public int getTimecardId() {
        return timecardId;
    }

    @Column(name = "selected_option")
    private String selectedOption;

    @Column(name = "start_date")
    private String startDate;

    @Column(name = "end_date")
    private String endDate;

    @Column(name = "monday")
    private int monday;

    @Column(name = "tuesday")
    private int tuesday;

    @Column(name = "wednesday")
    private int wednesday;

    @Column(name = "thursday")
    private int thursday;

    @Column(name = "friday")
    private int friday;

    @Column(name = "monday_mention")
    private String mondayMention;

    @Column(name = "tuesday_mention")
    private String tuesdayMention;

    @Column(name = "wednesday_mention")
    private String wednesdayMention;

    @Column(name = "thursday_mention")
    private String thursdayMention;

    @Column(name = "friday_mention")
    private String fridayMention;

    public int getMonday() {
        return monday;
    }


    public void setMonday(int monday) {
        this.monday = monday;
    }

    public int getTuesday() {
        return tuesday;
    }

    public void setTuesday(int tuesday) {
        this.tuesday = tuesday;
    }

    public int getWednesday() {
        return wednesday;
    }

    public void setWednesday(int wednesday) {
        this.wednesday = wednesday;
    }

    public int getThursday() {
        return thursday;
    }

    public void setThursday(int thursday) {
        this.thursday = thursday;
    }

    public int getFriday() {
        return friday;
    }

    public void setFriday(int friday) {
        this.friday = friday;
    }

    public void setUserId(Integer id) {
        this.userId = id;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setTimecardId(int timecardId) {
        this.timecardId = timecardId;
    }

    public void setSelectedOption(String selectedOption) {
        this.selectedOption = selectedOption;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setMondayMention(String mondayMention) {
        this.mondayMention = mondayMention;
    }

    public void setTuesdayMention(String tuesdayMention) {
        this.tuesdayMention = tuesdayMention;
    }

    public void setWednesdayMention(String wednesdayMention) {
        this.wednesdayMention = wednesdayMention;
    }

    public void setThursdayMention(String thursdayMention) {
        this.thursdayMention = thursdayMention;
    }

    public void setFridayMention(String fridayMention) {
        this.fridayMention = fridayMention;
    }
}