package com.alibou.security.auth;
import com.alibou.security.models.DataSentToTimeTrackingService;
import com.alibou.security.models.GetTimeAdminDto;
import com.alibou.security.models.WeeklyReport;
import com.alibou.security.user.Role;
import com.alibou.security.user.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;

@RestController
@RequestMapping("/api/v1/weeklyReport")
public class WeeklyReportReceiver {

    private final RestTemplate restTemplate;

    @Autowired
    public WeeklyReportReceiver(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    @GetMapping("/getAllWeeklyReport")
    public ResponseEntity<GetTimeAdminDto> getAllReport() throws JsonProcessingException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        if(!(user.getRole() == Role.ADMIN))
            return (ResponseEntity<GetTimeAdminDto>) ResponseEntity.badRequest();

        String url = "http://localhost:8081/api/bl/getAllTime";

        // Set request headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Create the request entity with headers
        HttpEntity<?> requestEntity = new HttpEntity<>(headers);

        // Send the GET request and wait for the response
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<GetTimeAdminDto> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                requestEntity,
                GetTimeAdminDto.class,
                user.getId()
        );

        // Get the response body
        GetTimeAdminDto responseBody = response.getBody();

        // Process the response as needed
        return ResponseEntity.ok(responseBody);
    }
    @PostMapping("/setReport")
    public ResponseEntity<String> getReport(@RequestBody LinkedHashMap<String, Object> reportRaw) throws JsonProcessingException {
        WeeklyReport report = new WeeklyReport( reportRaw);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();

        DataSentToTimeTrackingService data = new DataSentToTimeTrackingService(report,user.getId());

        String url = "http://localhost:8081/api/bl/postTimeCard" +
                ""; // Replace with the URL of the other Spring app

        // Set request headers

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Create the request entity with headers and body
        HttpEntity<DataSentToTimeTrackingService> requestEntity = new HttpEntity<>(data, headers); // Pass requestBody here if needed

        // Send the POST request and wait for the response
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);

        // Get the response body
        String responseBody = response.getBody();

        // Process the response as needed
        return ResponseEntity.ok(responseBody);

    }


    @GetMapping("/getHoursSum")
    public ResponseEntity<Integer> getReport() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();

        String url = "http://localhost:8081/api/bl/getTime/{userId}";

        // Set request headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Create the request entity with headers
        HttpEntity<?> requestEntity = new HttpEntity<>(headers);

        // Send the GET request and wait for the response
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Integer> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                requestEntity,
                Integer.class,
                user.getId()
        );

        // Get the response body
        Integer responseBody = response.getBody();

        // Process the response as needed
        return ResponseEntity.ok(responseBody);
    }
}