package com.alibou.security.models;

import com.alibou.security.Entities.WeeklyReportEntity;

import java.util.List;

public class GetTimeAdminDto {
    List<WeeklyReportEntity> list;

    public List<WeeklyReportEntity> getList() {
        return list;
    }

    public void setList(List<WeeklyReportEntity> list) {
        this.list = list;
    }

    public GetTimeAdminDto(List<WeeklyReportEntity> list) {
        this.list = list;
    }
    public GetTimeAdminDto() {
        // Default constructor
    }
}
