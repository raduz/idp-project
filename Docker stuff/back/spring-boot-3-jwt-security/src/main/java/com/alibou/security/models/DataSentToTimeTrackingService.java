package com.alibou.security.models;

import com.alibou.security.user.User;

public class DataSentToTimeTrackingService {
    private WeeklyReport report;

    private Integer id;

    @Override
    public String toString() {
        return "DataSentToTimeTrackingService{" +
                "report=" + report +
                ", id=" + id +
                '}';
    }

    public DataSentToTimeTrackingService(WeeklyReport report, Integer id) {
        this.report = report;
        this.id = id;
    }

    public WeeklyReport getReport() {
        return report;
    }

    public void setReport(WeeklyReport report) {
        this.report = report;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
