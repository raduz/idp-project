import { BrowserRouter, Routes, Route } from "react-router-dom";
import Form from "./Form";
import Layout from "./Layout";
import NoPage from "./NoPage";
import Auth from "./Auth";
import Home from "./Home";

const NavigationMenu = () =>{
    
    return (
        <>
            <div className="appContent">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout />}>
                        <Route path="time" element={<Form />} />
                        <Route path="auth" element={<Auth />} />
                        <Route path="home" element={<Home />} />
                        <Route path="*" element={<NoPage />} />
                    </Route>
                </Routes>
            </BrowserRouter>
            </div>
        </>
    );
}
export default NavigationMenu;