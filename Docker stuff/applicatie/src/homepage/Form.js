import { useState,useEffect } from "react";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
const Form = () => {
  const [clockedIn,setClockedIn] = useState(false);
  const [formData, setFormData] = useState({
    options: ["test1","test2"],
    selectedOption: "",
    startDate: getStartOfWeek(new Date()),
    endDate: getEndOfWeek(new Date()),
    monday: 0,
    tuesday: 0,
    wednesday: 0,
    thursday: 0,
    friday: 0,
    mondayMention: "",
    tuesdayMention: "",
    wednesdayMention: "",
    thursdayMention: "",
    fridayMention: ""
  });

  useEffect(() => {
    setFormData(formData => ({
      ...formData,
      selectedOption: formData.options[0]
    }));
  }, []);
  
  function handleOptionChange(event) {
      setFormData({
          ...formData,
          selectedOption: event.target.value
      });
  }

  const handleInputChange = (event) => {
      const { name, value } = event.target;
      setFormData({
          ...formData,
          [name]: value
      });
  };

  const handleDateChange = (dates) => {
      const [start, end] = dates;
      const selectedMonday = getStartOfWeek(new Date(start));
      const selectedFriday = getEndOfWeek(new Date(start));
      setFormData({
          ...formData,
          startDate: selectedMonday,
          endDate: selectedFriday
      });
  };

  const handleTimeChange = (event) => {
      const { name, value } = event.target;
      if (value >= 0 && value <= 24) {
          setFormData({
              ...formData,
              [name]: value
          });
      }
  };

  function getStartOfWeek(date) {
      const day = date.getDay();
      const diff = date.getDate() - day + (day === 0 ? -6 : 1);
      return new Date(date.setDate(diff));
  }

  function getEndOfWeek(date) {
      const day = date.getDay();
      const diff = date.getDate() + (5 - day);
      return new Date(date.setDate(diff));
  }

  const handleSubmit = (event) => {
      event.preventDefault();
      fetch('http://localhost:8080/api/v1/weeklyReport/setReport', {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${sessionStorage.getItem('access_token')}`
          }
      })
      .then(response => {
          console.log('Response:', response);
      })
      .catch(error => {
          console.error('Error:', error);
      });
  };


    return (
      <><div style={{ width: '50%', margin: '0 auto' }}>
      <form id="my-form" action="/submit-form" method="POST" onSubmit={handleSubmit}>
        
            <h1>Weekly Data Input</h1>
            <div>
            <select value={formData.options} onChange={handleOptionChange}>
              {formData.options.map(option => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
            </select>
            <p>You selected: {formData.selectedOption}</p>
          </div>
          <label>
            Select Week:
            <DatePicker
            selectsRange={true}
            startDate={formData.startDate}
            endDate={formData.endDate}
            onChange={handleDateChange}
            dateFormat="MM/dd/yyyy"
          />
          </label>
          <br />
          <label>
            Monday:
            <input type="number" name="monday" value={formData.monday} onChange={handleTimeChange} />
            Monday Mention:
            <input type="text" name="mondayMention" value={formData.mondayMention} onChange={handleInputChange} />
          </label>
          <br />
          <label>
            Tuesday:
            <input type="number" name="tuesday" value={formData.tuesday} onChange={handleTimeChange} />
            Tuesday Mention:
            <input type="text" name="tuesdayMention" value={formData.tuesdayMention} onChange={handleInputChange} />
          </label>
          <br />
          <label>
            Wednesday:
            <input type="number" name="wednesday" value={formData.wednesday} onChange={handleTimeChange} />
            Wednesday Mention:
            <input type="text" name="wednesdayMention" value={formData.wednesdayMention} onChange={handleInputChange} />
          </label>
          <br />
          <label>
            Thursday:
            <input type="number" name="thursday" value={formData.thursday} onChange={handleTimeChange} />
            Thursday Mention:
            <input type="text" name="thursdayMention" value={formData.thursdayMention} onChange={handleInputChange} />
          </label>
          <br />
          <label>
            Friday:
            <input type="number" name="friday" value={formData.friday} onChange={handleTimeChange} />
            Friday Mention:
            <input type="text" name="fridayMention" value={formData.fridayMention} onChange={handleInputChange} />
          </label>
          <br />
          <button type="submit">Submit</button>
      </form>
        </div>
    </>)
}

export default Form;