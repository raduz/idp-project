import { Outlet, Link } from "react-router-dom";

const Layout = () => {
  return (
    <>
        <div className="header">
            <nav>
                <ul className="navbar" label="flexDirection">
                    <li className="navbarPage" id="navbarPageElement1">
                        <Link to="/home" className="navTitle">Home</Link>
                    </li>
                    <li className="navbarPage" id="navbarPageElement2">
                        <Link to="/time" className="navTitle">time tracking</Link>
                    </li>
                    <li className="navbarPage" id="navbarPageElement3">
                        <Link to="/auth" className="navTitle">Authentication</Link>
                    </li>
                </ul>
            </nav>
        </div>

      <Outlet />
    </>
  )
};

export default Layout;